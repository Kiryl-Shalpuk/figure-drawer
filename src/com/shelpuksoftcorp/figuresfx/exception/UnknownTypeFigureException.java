package com.shelpuksoftcorp.figuresfx.exception;

public class UnknownTypeFigureException extends Exception {

    public UnknownTypeFigureException() {
        printStackTrace();
    }

    @Override
    public String toString() {
        return "com.shelpuksoftcorp.figuresfx.exception.UnknownTypeFigureException";
    }
}
