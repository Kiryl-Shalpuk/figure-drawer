package com.shelpuksoftcorp.figuresfx.exception;

public class EmptyArrayException extends Exception {
    public EmptyArrayException() {
        printStackTrace();
    }

    @Override
    public String toString() {
        return "com.shelpuksoftcorp.figuresfx.exception.EmptyArrayException";
    }
}
