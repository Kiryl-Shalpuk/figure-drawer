package com.shelpuksoftcorp.figuresfx.controller;

import com.shelpuksoftcorp.figuresfx.drawUtils.Drawer;
import com.shelpuksoftcorp.figuresfx.exception.EmptyArrayException;
import com.shelpuksoftcorp.figuresfx.exception.UnknownTypeFigureException;
import com.shelpuksoftcorp.figuresfx.figures.*;
import com.shelpuksoftcorp.figuresfx.figures.Rectangle;
import javafx.embed.swing.SwingFXUtils;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.image.WritableImage;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import org.apache.log4j.Logger;
import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;

public class MainScreenViewController implements Initializable {

    private List<Figure> figures = new ArrayList<>();
    private Random random;

    private static final Logger log = Logger.getLogger(Logger.class);

    @FXML
    private Canvas canvas;
    @FXML
    public Button clear_button;
    @FXML
    public Button save_button;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        random = new Random(System.currentTimeMillis());
    }

    private void addFigure(Figure figure) {
        figures.add(figure);
        return;
    }

    private Figure createFigure(double x, double y) throws UnknownTypeFigureException {
        Figure figure;
        switch (getIntRandom(5)) {
            case Figure.FIGURE_TYPE_CIRCLE:
                figure = new Circle(x, y, getIntRandom(5), Color.RED, getIntRandom(50));
                break;
            case Figure.FIGURE_TYPE_RECTANGLE:
                figure = new Rectangle(x, y, getIntRandom(5), Color.BLUE, getIntRandom(50), getIntRandom(50));
                break;
            case Figure.FIGURE_TYPE_TRIANGLE:
                figure = new Triangle(x, y, getIntRandom(5), Color.GREEN, getIntRandom(50));
                break;
            case Figure.FIGURE_TYPE_STAR:
                figure = new Star(x, y, getIntRandom(5), Color.YELLOW, getIntRandom(50));
                break;
            case Figure.FIGURE_TYPE_HEART:
                figure = new Heart(x, y, getIntRandom(5), Color.PURPLE, getIntRandom(50));
                break;
            default:
                throw new UnknownTypeFigureException();
        }
        return figure;
    }

    private void repaint() throws EmptyArrayException {
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        Drawer<Figure> drawer = new Drawer<>(figures);
        if (drawer == null) {
            throw new EmptyArrayException();
        }
        drawer.draw(canvas.getGraphicsContext2D());
    }

    public int getIntRandom(int r) {
        return random.nextInt(r);
    }

    @FXML
    private void onCanvasMouseClicked(MouseEvent mouseEvent) {
        try {
            addFigure(createFigure(mouseEvent.getX(), mouseEvent.getY()));
            repaint();
        } catch (UnknownTypeFigureException e) {
            log.info("Type of figure does not match...");
        } catch (EmptyArrayException f) {
            log.info("Figure's array is empty...");
        }
    }

    @FXML
    public void onClearButtonMouseClicked() {
        canvas.getGraphicsContext2D().clearRect(0, 0, canvas.getWidth(), canvas.getHeight());
        figures = new ArrayList<>();
    }

    @FXML
    public void onSaveButtonMouseClicked() {
        String imagePath = ("res/image/");
        String imageName = ("Image(" + new SimpleDateFormat("yyyy-MM-dd-hh-mm-ss").format(new Date()) + ").png");
        WritableImage writableImage = new WritableImage(1024, 600);
        canvas.snapshot(null, writableImage);
        File file = new File(imagePath + imageName);
        try {
            ImageIO.write(SwingFXUtils.fromFXImage(writableImage, null), "png", file);
        } catch (IOException e) {
            log.info("Error of creating image...");
            e.printStackTrace();
        }
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle(imageName);
        alert.setHeaderText(null);
        alert.setContentText("Scene has saved. Follow to the next folder: \nfigure-drawer/" + imagePath);
        alert.show();
    }
}
