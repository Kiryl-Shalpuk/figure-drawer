package com.shelpuksoftcorp.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import org.apache.commons.lang3.ArrayUtils;
import java.util.*;

public class Heart extends Figure {
    private double radius;
    private List<Double> xCoord = new ArrayList<>();
    private List<Double> yCoord = new ArrayList<>();

    public Heart(double cx, double cy, double lineWidth, Color color) {
        super(FIGURE_TYPE_HEART, cx, cy, lineWidth, color);
    }

    public Heart(double cx, double cy, double lineWidth, Color color, double radius) {
        this(cx, cy, lineWidth, color);
        this.radius = radius < 10 ? 10 : radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Heart heart = (Heart) o;
        return Double.compare(heart.radius, radius) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(radius);
    }

    @Override
    public String toString() {
        return "com.shelpuksoftcorp.figuresfx.figures.Heart{" +
                "radius=" + radius +
                '}';
    }

    @Override
    public void draw(GraphicsContext gc) {
        funcXY();
        double[] x = ArrayUtils.toPrimitive(xCoord.toArray(new Double[xCoord.size()]));
        double[] y = ArrayUtils.toPrimitive(yCoord.toArray(new Double[yCoord.size()]));
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokePolyline(x, y, x.length);
        xyClear();
    }

    public void funcXY() {
        List<Double> xPlusCoord = new ArrayList<>();
        List<Double> xMinusCoord = new ArrayList<>();
        List<Double> firstYCoord = new ArrayList<>();
        List<Double> secondYCoord = new ArrayList<>();
        for (double x = -1.139; x <= 0; x = x + 0.01) {
            double y1 = (Math.pow(Math.abs(x), 2.0 / 3.0) + Math.sqrt(Math.pow(Math.abs(x), 4.0 / 3.0) - 4.0 * Math.pow(Math.abs(x), 2.0) + 4.0)) / 2.0;
            double y2 = (Math.pow(Math.abs(x), 2.0 / 3.0) - Math.sqrt(Math.pow(Math.abs(x), 4.0 / 3.0) - 4.0 * Math.pow(Math.abs(x), 2.0) + 4.0)) / 2.0;
            xPlusCoord.add(cx + x * radius);
            xMinusCoord.add(cx - x * radius);
            firstYCoord.add(cy - y1 * radius);
            secondYCoord.add(cy - y2 * radius);
        }
        xCoord.addAll(xPlusCoord);
        yCoord.addAll(firstYCoord);
        Collections.reverse(xMinusCoord);
        Collections.reverse(firstYCoord);
        xCoord.addAll(xMinusCoord);
        yCoord.addAll(firstYCoord);
        Collections.reverse(xMinusCoord);
        xCoord.addAll(xMinusCoord);
        yCoord.addAll(secondYCoord);
        Collections.reverse(xPlusCoord);
        Collections.reverse(secondYCoord);
        xCoord.addAll(xPlusCoord);
        yCoord.addAll(secondYCoord);
    }

    public void xyClear() {
        xCoord.clear();
        yCoord.clear();
    }
}
