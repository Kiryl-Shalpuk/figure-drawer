package com.shelpuksoftcorp.figuresfx.figures;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;

import java.util.Objects;

public class Star extends Figure {
    private double radius;

    public Star(double cx, double cy, double lineWidth, Color color) {
        super(FIGURE_TYPE_STAR, cx, cy, lineWidth, color);
    }

    public Star(double cx, double cy, double lineWidth, Color color, double radius) {
        this(cx, cy, lineWidth, color);
        this.radius = radius < 10 ? 10 : radius;
    }

    public double getRadius() {
        return radius;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Star star = (Star) o;
        return Double.compare(star.radius, radius) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(radius);
    }

    @Override
    public String toString() {
        return "com.shelpuksoftcorp.figuresfx.figures.Star{" +
                "radius=" + radius +
                '}';
    }

    public double sin(double angle) {
        angle = Math.sin(Math.toRadians(angle));
        return angle;
    }

    public double cos(double angle) {
        angle = Math.cos(Math.toRadians(angle));
        return angle;
    }

    @Override
    public void draw(GraphicsContext gc) {
        gc.setLineWidth(lineWidth);
        gc.setStroke(color);
        gc.strokePolygon(
                new double[]{
                        cx,
                        cx - sin(36) * radius / 2,
                        cx - cos(18) * radius,
                        cx - cos(18) * radius / 2,
                        cx - sin(36) * radius,
                        cx,
                        cx + sin(36) * radius,
                        cx + cos(18) * radius / 2,
                        cx + cos(18) * radius,
                        cx + sin(36) * radius / 2
                }, new double[]{
                        cy - radius,
                        cy - cos(36) * radius / 2,
                        cy - sin(18) * radius,
                        cy + sin(18) * radius / 2,
                        cy + cos(36) * radius,
                        cy + radius / 2,
                        cy + cos(36) * radius,
                        cy + sin(18) * radius / 2,
                        cy - sin(18) * radius,
                        cy - cos(36) * radius / 2
                }, 10
        );
    }
}
