package com.shelpuksoftcorp.figuresfx.drawUtils;

import javafx.scene.canvas.GraphicsContext;

public interface Drawable {
    void draw(GraphicsContext gc);
}
