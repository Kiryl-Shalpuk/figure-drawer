package com.shelpuksoftcorp.figuresfx.drawUtils;

import com.shelpuksoftcorp.figuresfx.figures.Figure;
import javafx.scene.canvas.GraphicsContext;
import java.util.List;

public class Drawer<T extends Figure & Drawable> {
    List<T> figures;

    public Drawer(List<T> figures) {
        this.figures = figures;
    }

    public void draw(GraphicsContext gc) {
        if (figures != null && figures.size() != 0) {
            for (T f : figures) {
                f.draw(gc);
            }
        }
    }
}
